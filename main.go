package main

import (
	"fmt"
	"log"
	"net/http"

	"google.golang.org/api/idtoken"
)

func main() {
	r := http.NewServeMux()
	r.HandleFunc("/callback", func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		token := r.FormValue("credential")
		fmt.Println(token)
		payload, err := idtoken.Validate(r.Context(), token, "")
		if err != nil {
			log.Print(err)
			return
		}
		http.SetCookie(w, &http.Cookie{
			Name:  "GOGOL-MOGOL",
			Value: "123",
		})
		// log.Printf("Method: %v, Host: %v, Header: %+v", r.Method, r.Host, r.Header)
		fmt.Fprintln(w, "Response from Google:")
		fmt.Fprintf(w, "Issuer: %v\n", payload.Issuer)
		fmt.Fprintf(w, "Audience: %v\n", payload.Audience)
		fmt.Fprintf(w, "Expires: %v\n", payload.Expires)
		fmt.Fprintf(w, "IssuedAt: %v\n", payload.IssuedAt)
		fmt.Fprintf(w, "Subject: %v\n", payload.Subject)
		fmt.Fprintf(w, "Claims: %+v\n", payload.Claims)
	})

	r.Handle("/", http.FileServer(http.Dir(".")))
	r.HandleFunc("/check", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "<html><body><h3>Cookies:</h3><ul>")
		for _, cookie := range r.Cookies() {
			fmt.Fprintf(w, "<li>Name: %s Value: %s</li>", cookie.Name, cookie.Value)
		}
		fmt.Fprint(w, "</ul></body></html>")
	})
	log.Print("Listening at :3000")
	http.ListenAndServe(":3000", r)
}
